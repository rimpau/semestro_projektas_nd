package com.company;
/*
    input:
    ${vardas} ${pavardė} ${bakalaurantas|magistrantas|doktorantas} ${mokslas} ${vidurkis}

    output:
    ${pilnas_vardas} gaus ${stipendija} € stipendiją

    conditions:
    Bakalaurantai gauna 100 € stipendiją, jei vidurkis
    didesnis už 8
    Magistrantai gauna 200 € stipendiją, jei studijuoja
    tiksliuosius mokslus
    Doktorantai gauna 800 € stipendiją bet kokiu atveju
    Prie doktorantų vardo turi būti prirašyta „Dr.“

    panaudoti:
    enkapsuliacija
    paveldimumą
    polimorfizmą.
*/

import java.util.Arrays;
import java.util.Scanner;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        System.out.println ( "Input ${name} ${last name} ${bachelors|masters|phd} ${field} ${average grade}" );
        Scanner scanner = new Scanner ( System.in );
        Student student = new Student ( scanner.next (), scanner.next (), scanner.next (), scanner.next (), scanner.nextInt () );
        System.out.printf ( "%s will get %d$ scholarship" , student.getFullName (), student.getScholarship ());
    }

    public static class Program {
        private final String degree;
        private final String field;
        public final String[] appliedFields = new String[] {"mathematics","physics","engineering"};

        public Program(String degree, String field) {
            this.degree = degree;
            this.field = field;
        }

        public String getField() {
            return field;
        }

        public String getDegree() {
            return degree;
        }

        public boolean isApplied() {
            List<String> list = Arrays.asList (appliedFields);
            return list.contains ( field );
        }
    }

    public static class Student extends Program {
        private final String firstName;
        private final String lastName;
        private final int averageGrade;

        public Student(String firstName, String lastName, String degree, String field, int averageGrade) {
            super ( degree, field );
            this.firstName = firstName;
            this.lastName = lastName;
            this.averageGrade = averageGrade;
        }

        public String getFullName() {
            if (getDegree ().equals ( "phd" )) {
                return "Dr. " + firstName + " " + lastName;
            } else
                return firstName + " " + lastName;
        }

        public int getScholarship() {
            int amount = 0;
            switch (getDegree ()) {
                case "bachelors":
                    if (averageGrade > 8) {
                        amount = 100;
                    }
                    break;
                case "masters":
                    if (isApplied ()) {
                        amount = 200;
                    }
                    break;
                case "phd":
                    amount = 800;
                    break;
                default:
                    break;
            }
            return amount;
        }
    }
}